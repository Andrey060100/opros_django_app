from django.contrib import admin

from .models import Question, Answers,NameTests

class AnswerInline(admin.StackedInline):
    model = Answers
    extra = 0
    list_display = ('title', 'topic','correct')


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [(None,{'fields':['title','topic','id_test']})]
    inlines = [AnswerInline]
    list_display = ('title','topic','id_test')

class NameTestsAdmin(admin.ModelAdmin):
    model = NameTests
    list_display = ['title_test','test_professions']



admin.site.register(Question, QuestionAdmin)
admin.site.register(NameTests, NameTestsAdmin)
