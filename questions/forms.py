from django.forms import ModelForm, CheckboxSelectMultiple,ModelChoiceField
from .models import Question,Answers


class AnswersForm(ModelForm):
    answer  = ModelChoiceField(queryset=Answers.objects.only('answer'),widget=CheckboxSelectMultiple)
    class Meta:
        model = Answers
        fields = ('answer',)
        widgets = {'answer':CheckboxSelectMultiple}






